Code Dish

https://github.com/olange/learning-mongodb/blob/master/course-m101p/hw3-3-blogPostDAO.py

overview of building an app:
	# mongodb:- it is  database ( notation is similar like json, bascially it is in bson format)
	# mongo:- mongo is a shell which will connect the mongodb
	# pymongo:- it is a module which in used for connecting the the mongodb via python 
	# bottleneck:- it will be used as a framework, to make a blog

installing a mongodb(ubuntu)
	# install mongodb
		# https://www.digitalocean.com/community/tutorials/how-to-install-mongodb-on-ubuntu-14-04
	# run service mongod start
	# run mongo 
		# db.names.insert({"name":"Andrew Erlichson"})
		# db.names.find()

JSON
	# Json support object, array, string, number as values 
	# 

BSON
	# it has following features 
		# application driver (pymongo) to mongodb i.e native datatype to bson
		# bson to nativedatatype i.e mongodb to nativedatatype 

	# Bson (binary json)
		# ligthweight 
		# traversal 
		# efficient 

	# json it decoded 
		# hello to "\x16\x00\x00\x00\x02hello\x00 

	# True or false? BSON plays the role of a canonical (i.e., "unique") representation of documents shared across all drivers and tools. (True)

	# it supports date, integer as values types also with object, array, string, number
	# {
		  "headline" : "Apple Reported Fourth Quarter Revenue Today",
		  "date" : ISODate("2015-10-27T22:35:21.908Z"),
		  "views" : 1132,
		  "author" : {
		    "name" : "Bob Walker",
		    "title" : "Lead Business Editor"
		  },
		  "published" : true,
		  "tags" : [ 
		    "AAPL", 
		    { "name" : "city", "value" : "Cupertino" },
		    [ "Electronics", "Computers" ]
		  ]
		}

intoduction to crud
	# create, read, update, delete
	# commands
		# help 
		# show dbs
		# show collections 
		# use <dbname>
			# it creates db if not exit 
		# db.movies.insert({ "title": "Jaws", "year": 1975, "imdb": "tt0073195" });
		# db.movies.insert({ "title": "Mad Max 2: The Road Warrior", "year": 1981, "imdb": "tt0082694" })
		# db.movies.insert({ "title": "Raiders of the Lost Ark", "year": 1981, "imdb": "tt0082971" })

		# db.movies.find()

		# db.movies.find().pretty()

		# db.movies.find({"title" : "Jaws"})

		# var c = db.movies.find({"year" : "1981"})

		# c.hasNext()
			# True / False

		# c.next()


Application Architecture Overviews 
	# mongod (process)
	# mongo (shell)
	# bottleneck (framework)
	# pymongod (python driver for mongodb communication)
	# BSON (as json )
	# HTTP for request 

install python 
	# msi install 
	# set path of python 
	# set path for pip 

introduction to bottle web framework 
	# pip install bottle 

	from bottle import route, run, template

	@route('/hello/<name>')
	def index(name):
	    return template('<b>Hello {{name}}</b>!', name=name)

	run(host='localhost', port=8080)

install pymongo 
	# go to api.mongodb.org
	# install beta version 
		# http://api.mongodb.com/python/current/installation.html
		# python -m pip install https://github.com/mongodb/mongo-python-driver/archive/3.2rc0.tar.gz (not worked)
		# sudo pip install pymongo 	

hello world mongo style 
	# open mongo 
	# run command 
		# var j = db.names.findOne()
		# j.name = "Dwight"
		# db.name.save(j)

		db.names.save 
			# gived the javascript code 


hello world on web server 
	# arcitecture Screenshot from 2016-05-25 23:08:12.png

	import bottle
	import pymongo

	# this is the handler for the default path of the web server

	@bottle.route('/')
	def index():
	    
	    # connect to mongoDB
	    connection = pymongo.MongoClient('localhost', 27017)

	    # attach to test database
	    db = connection.test


	    # get handle for names collection
	    name = db.names

	    # find a single document
	    item = name.find_one()

	    return '<b>Hello %s!</b>' % item['name']


	bottle.run(host='localhost', port=8082)

	# browser
		# http://localhost:8082/


Introduction to Our Class Project, The Blog
	

Blog in Relational Tables Quiz
	# Blog in Relational Tables
	# Screenshot from 2016-05-26 19:26:26.png
	# Screenshot from 2016-05-26 19:27:31.png 

	# relational tables
	# so to access the post with tags and  comments we have to fetch 6 tables

	authors:
	    author_id,
	    name,
	    email,
	    password

	posts:
	    post_id,
	    author_id
	    title,
	    body,
	    publication_date

	comments:
	    comment_id,
	    name,
	    email,
	    comment_text

	post_comments:
	    post_id,
	    comment_id


	tags
	    tag_id
	    name

	post_tags
	    post_id
	    tag_id



Blog in Documents Quiz

	# Screenshot from 2016-05-26 19:44:30.png
	# post
	{
		'title':'....', 
		"author": "....", 
		"date": '....',
		"comments":[{
		              "name":'...', 
		              "email":"....", 
		              "comment":"...."
		           }], 
		"tags":['....', '....', '....']
	}

	authors
	{
	    "_id":'....', 
	    "password":'....'
	}

	quize:
		how many collection we will reuired to bild a blog 
		anwser = 1



Introduction to Schema Design
	# we need to make diffrennt collection if the data exceed more than 16 MB 



Introduction to Python Quiz
	# it is readable
	# garbage collective 
	# dynamic typed
	# bunch of more things 


Python Lists Quiz

Python Lists, Slice Operator Quiz

Python Lists, Inclusion Quiz

Python, Working with Dicts Quiz
    # diffrence b/w dict an json is that dict do not maintain the order 


Bottle Framework: URL Handlers
	# 


Bottle Framework: Using Views
	# Screenshot from 2016-05-26 21:17:32.png
	# model ( monog db , business logic )
	# views (which shows by the user )
	# control (which takes the input from user and )


###########

week 2end 

upgrade mongodb 3.0 to 3.2
	# https://www.anintegratedworld.com/how-to-install-or-upgrade-mongodb-on-ubuntu/

create document and insert collection
	# inserOne
	# insertMany


_id
	12 byte hex string 
	# 4-3-2-3 (date, mac addredd, pid, counter)

reading document 
	# on th entire arrray 
	# based on any elemtent 
	# based on a specific element 
	# more complex matches using operator

	# cursor 
		var c = collection.find()
		vad doc = function(){
		c.hasNext() ? c.next: null
		}
	# procedure 
		# exclude and include

		# collection.find({
		"name":"jai"
		}, {"title", 1})


comparision 
	# query  comparision of mongo db 
	# https://docs.mongodb.com/manual/reference/operator/query/#query-selectors
	# http://api.mongodb.com/python/current/api/pymongo/collection.html#pymongo.collection.Collection.group

element 
	# element oprator 
	# db.moviesDetails.find({ "tomato.meter": { $exists: true } })

	db.moviesDetails.find({ "tomato.meter": { $exists: false } })

	// Value of $type may be either a BSON type number or the string alias
	// See https://docs.mongodb.org/manual/reference/operator/query/type
	db.moviesScratch.find({ _id: { $type: "string" } })


logical oprator 
	# and , or, not, nor
	# and is used when we do read multifle vaue for sam field

regix
	# db.movieDetails.find({ "awards.text": { $regex: /^Won.*/ } }).pretty()

	db.movieDetails.find({ "awards.text": { $regex: /^Won.*/ } },
	                     { title: 1, "awards": 1, _id: 0}).pretty()

array
	db.movieDetails.find({ genres: { $all: ["Comedy", "Crime", "Drama"] } }).pretty()

	db.movieDetails.find({ countries: { $size: 1 } }).pretty()

	boxOffice: [ { "country": "USA", "revenue": 41.3 },
	             { "country": "Australia", "revenue": 2.9 },
	             { "country": "UK", "revenue": 10.1 },
	             { "country": "Germany", "revenue": 4.3 },
	             { "country": "France", "revenue": 3.5 } ]

	db.movieDetails.find({ boxOffice: { country: "UK", revenue: { $gt: 15 } } })

	db.movieDetails.find({ boxOffice: {$elemMatch: { country: "UK", revenue: { $gt: 15 } } } })


homework 2.1
	# mongoimport -d students -c grades < grades.json
	# db.grades.aggregate({'$group':{'_id':'$student_id', 'average':{$avg:'$score'}}}, {'$sort':{'average':-1}}, {'$limit':1})

	# db.grades.find({"type":"exam", $or: [{"score":{"$gte":65}}]}).sort({"score":1})
	# 22

homework 2.2
	# aggregate
		# https://docs.mongodb.com/manual/aggregation/#Aggregation-Group
		# https://bigmongodb.wordpress.com/2016/01/19/mongodb-university-m101p-chapter-2-homework/

	# that will remove the grade of type "homework"
	# lowest score for each student
	# remove one document per student

	db.grades.aggregate({$match:{"type":"homework"}}, 
	{'$group':{'_id':'$student_id', 'minitem':{$min:'$score'}}}, {'$sort':{'average':-1}})

	myresults = grades.aggregate( [{ '$match': {'type': "homework" } }, { "$group": { '_id':'$student_id' , 'minitem': { '$min': "$score" } } } , {"$sort":{'_id':1}}] )


	import pymongo
	connection = pymongo.MongoClient("mongodb://localhost")
	db = connection.students
	grades = db.grades.find({'type': 'homework'}).sort([('student_id', 1), ('score', 1)])
	student_id = -1


	# Validation Code is  jkfds5834j98fnm39njf0920f02


homework 2.4 
	#  mongorestore --drop -d test -c movieDetails movieDetails.bson

homework 2.5

	#  db.movieDetails.find({"countries.1": "Sweden"}).pretty()
 


	for g in grades:
		if student_id != g['student_id']:
			student_id = g['student_id']
			score = g['score']
			print g
			print "removed student_id", student_id
			db.grades.remove({'student_id': student_id, 'score' : score})


homework4

	# http://www.techiekunal.in/2015/11/m101j-mongodb-for-java-developers-week-4-homework.html
	# https://github.com/olange/learning-mongodb/blob/master/course-m101p/hw4-3-answer.md

	4.3 
		# db.posts.ensureIndex({ date: -1})
		# db.posts.ensureIndex({ permalink: 1}, {unique: true})
		# db.posts.ensureIndex( { tags:1, date: -1})

