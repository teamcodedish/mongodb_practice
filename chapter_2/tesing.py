# import pymongo
# import sys

# # establish a connection to the database
# connection = pymongo.MongoClient("mongodb://localhost")

# # get a handle to the school database
# db=connection.students
# grades = db.grades

# myresults = grades.aggregate( [{ '$match': {'type': "homework" } }, { "$group": { '_id':'$student_id' , 'minitem': { '$min': "$score" } } } , {"$sort":{'score':1}}] )

# for i in myresults:
#     print i["_id"]
#     grades.delete_one({ 'student_id': i["_id"]})

import pymongo
connection = pymongo.MongoClient("mongodb://localhost")
db = connection.students
grades = db.grades.find({'type': 'homework'}).sort([('student_id', 1), ('score', 1)])
student_id = -1

for g in grades:
	if student_id != g['student_id']:
		student_id = g['student_id']
		score = g['score']
		print g
		print "removed student_id", student_id
		db.grades.remove({'student_id': student_id, 'score' : score})